import 'package:flutter/material.dart';
import 'home.dart';
import 'person.dart';

class SendScreen extends StatelessWidget {
  final Person person;

  // In the constructor, require a Todo
  SendScreen({Key key, @required this.person}) : super(key: key);

  final TextStyle textStyle = TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.w400,
    color: Colors.white,
    // shadows: [
    //   BoxShadow(
    //     color: Colors.grey[800],
    //     blurRadius: 3.0, // has the effect of softening the shadow
    //     spreadRadius: 20.0, // has the effect of extending the shadow
    //     offset: Offset(
    //       1.0, // horizontal, move right 10
    //       1.0, // vertical, move down 10
    //     ),
    //   )
    // ]
  );

  final TextStyle textStyle2 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w400,
    color: Colors.grey,
  );

  final TextStyle textStyle3 = TextStyle(
    fontSize: 38,
    fontWeight: FontWeight.w400,
    color: Colors.grey[800],
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        brightness: Brightness.dark,
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Text(person.name),
      ),
      body: ListView.builder(
          itemCount: 1,
          itemBuilder: (context, index) {
            return Column(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                    height: 200,
                    child: ChildrenWidget(
                      person: person,
                    )),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 16, top: 20),
                  child: SizedBox(
                      height: 90,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xffd7b48b),
                            // border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16.0),
                                topRight: Radius.circular(16))),
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Row(
                            children: <Widget>[
                              Text("Add money", style: textStyle),
                              // Text(person.balance, style: textStyle),
                            ],
                          ),
                        ),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                  ),
                  child: SizedBox(
                      height: 90,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          // border: Border.all(color: Colors.grey),
                          // borderRadius: BorderRadius.only(
                          //     topLeft: Radius.circular(16.0),
                          // topRight: Radius.circular(16))
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Amount",
                                    textAlign: TextAlign.left,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 18.0),
                                    child: SizedBox(
                                      width: 300,
                                      child: TextField(
                                        keyboardType: TextInputType.numberWithOptions(),
                                        decoration: InputDecoration(
                                            // border: InputBorder.none,
                                            hintText: 'CHF 0.-'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16.0,
                    right: 16,
                  ),
                  child: SizedBox(
                      height: 90,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          // border: Border.all(color: Colors.grey),
                          // borderRadius: BorderRadius.only(
                          //     topLeft: Radius.circular(16.0),
                          // topRight: Radius.circular(16))
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 12.0, right: 12),
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Message",
                                    textAlign: TextAlign.left,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 18.0),
                                    child: SizedBox(
                                      width: 300,
                                      child: TextField(
                                        decoration: InputDecoration(
                                            // border: InputBorder.none,
                                            hintText: 'Type your mesage 😀'),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: ButtonTheme(
                      minWidth: 340.0,
                      height: 40.0,
                      child: RaisedButton(
                        textColor: Colors.white,
                        splashColor: Color(0xffd7b48b),
                        color: Color(0xff4fc8bb),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              // return object of type Dialog
                              return AlertDialog(
                                title: new Text("Success 😎"),
                                content: new Text(
                                    "The money was sent to: " + person.name),
                                actions: <Widget>[
                                  // usually buttons at the bottom of the dialog
                                  ButtonTheme(
                                      minWidth: 260.0,
                                      height: 40.0,
                                      child: RaisedButton(
                                        child: Text("👍"),
                                        textColor: Colors.white,
                                        textTheme: ButtonTextTheme.primary,
                                        splashColor: Color(0xffd7b48b),
                                        color: Color(0xff4fc8bb),
                                        onPressed: () {
                                          Navigator.of(context).pop();

                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    PersonScreen(
                                                      person: Person(
                                                        transactions: person.transactions,
                                                          balance: "102.- CHF",
                                                          bigPic: person.bigPic,
                                                          name: person.name,
                                                          pic: person.pic),
                                                    )),
                                          );
                                        },
                                      )),
                                ],
                              );
                            },
                          );
                        },
                        child: const Text('Send'),
                      )),
                ),
              ],
            );
          }),
    );
  }
}

class ChildrenWidget extends StatelessWidget {
  final Person person;

  // In the constructor, require a Todo
  ChildrenWidget({Key key, @required this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        ChildrenItem(
          pic: person.bigPic,
          width: 350,
        ),
      ],
    );
  }
}
