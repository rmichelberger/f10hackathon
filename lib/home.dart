import 'package:flutter/material.dart';
import 'person.dart';

class Home extends StatelessWidget {
  static List<Transaction> items = [
    Transaction(
        title: 'Coop Hauptbahnhof',
        amount: "15.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Hairstylist of the Universe',
        amount: "116.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Hilton Food', amount: "56.- CHF", date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Mobility carsharing',
        amount: "16.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Coop Hauptbahnhof',
        amount: "56.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Migros Hauptbahnhof',
        amount: "56.- CHF",
        date: "22.03.2019, 10:23"),
  ];

  final TextStyle textStyle = TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.w400,
    color: Colors.white,
    // shadows: [
    //   BoxShadow(
    //     color: Colors.grey[800],
    //     blurRadius: 3.0, // has the effect of softening the shadow
    //     spreadRadius: 20.0, // has the effect of extending the shadow
    //     offset: Offset(
    //       1.0, // horizontal, move right 10
    //       1.0, // vertical, move down 10
    //     ),
    //   )
    // ]
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Pocket money'),
        brightness: Brightness.dark,
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(height: 200, child: ChildrensWidget()),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16, top: 20),
            child: SizedBox(
                height: 90,
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xffc6d374),
                      // border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16))),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          "Your last transactions",
                          style: textStyle,
                        ),
                        Text(""),
                      ],
                    ),
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16),
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  // // border: Border.all(color: Colors.grey),
                  // borderRadius: BorderRadius.only(
                  //     bottomLeft: Radius.circular(16.0),
                  //     bottomRight: Radius.circular(16.0))
                ),
                height: 245,
                child: TransactionWidget(
                  items: items,
                )),
          ),
        ],
      ),

      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     // Navigator.push(
      //     //   context,
      //     //   MaterialPageRoute(
      //     //       builder: (context) => PersonScreen(
      //     //             person: Person(),
      //     //           )),
      //     // );
      //   },
      //   tooltip: 'in',
      //   child: Icon(Icons.add),
      // ), //
    );
  }
}

class ChildrenItem extends StatelessWidget {
  ChildrenItem({Key key, this.pic, this.width}) : super(key: key);

  final String pic;
  final double width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // width: 250,
      // height: 250,
      child: Stack(
        children: <Widget>[
          Container(
            width: width,
            alignment: Alignment.center,
            height: 200,
            padding: EdgeInsets.only(top: 20),
            // color: Colors.red,
            child: Image.asset(pic),
          ),
        ],
      ),
    );
  }
}

class ChildrensWidget extends StatelessWidget {
  static List<Transaction> sinaT = [
    Transaction(title: 'Sweets', amount: "5.- CHF", date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Toys',
        amount: "16.- CHF",
        date: "22.03.2019, 10:23",
        message: true),
    Transaction(
        title: 'Rivella', amount: "1.5.- CHF", date: "22.03.2019, 10:23"),
    Transaction(title: 'Puppy', amount: "25.- CHF", date: "22.03.2019, 10:23"),
  ];

  static List<Transaction> julianT = [
    Transaction(title: 'Lunch', amount: "15.- CHF", date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Hairstylist of the Universe',
        amount: "116.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'PlayStation 1', amount: "56.- CHF", date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Mobility carsharing',
        amount: "16.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Coop Hauptbahnhof',
        amount: "56.- CHF",
        date: "22.03.2019, 10:23"),
    Transaction(
        title: 'Migros Hauptbahnhof',
        amount: "56.- CHF",
        date: "22.03.2019, 10:23"),
  ];

  static List<Person> persons = [
    Person(
        pic: 'assets/Sina_small.png',
        bigPic: 'assets/Sina_large.png',
        balance: "15.- CHF",
        name: "Sina",
        transactions: sinaT),
    Person(
        pic: 'assets/Boy_small.png',
        bigPic: 'assets/Boy_large.png',
        balance: "72.- CHF",
        name: "Julian",
        transactions: julianT)
  ];

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PersonScreen(
                          person: persons[0],
                        )),
              );
            },
            child: ChildrenItem(
              pic: persons[0].pic,
              width: 170,
            )),
        GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PersonScreen(
                          person: persons[1],
                        )),
              );
            },
            child: ChildrenItem(
              pic: persons[1].pic,
              width: 170,
            )),
      ],
    );
  }
}

class Person {
  String pic;
  String bigPic;
  String balance;
  String name;

  final List<Transaction> transactions;

  Person({this.pic, this.bigPic, this.balance, this.name, this.transactions})
      : super();
}

class Transaction {
  String title;
  String amount;
  String date;
  bool message;

  Transaction({this.title, this.amount, this.date, this.message = false})
      : super();
}

class TransactionWidget extends StatelessWidget {
  final List<Transaction> items;

  // In the constructor, require a Todo
  TransactionWidget({Key key, @required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(
              color: Colors.grey,
            ),
        itemCount: items.length,
        itemBuilder: (BuildContext ctxt, int index) {
          Transaction transaction = items[index];
          return ListTile(
            // leading: Icon(transaction.icon),
            leading: transaction.message
                ? IconTheme(
                    data: IconThemeData(color: Color(0xff4fc8bb),),
                    child: Icon(Icons.chat_bubble),
                  )
                : null,
            title: Text(transaction.title),
            trailing: Text(transaction.amount),
            subtitle: Text(transaction.date),
          );
        });
  }
}
