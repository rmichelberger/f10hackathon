import 'package:flutter/material.dart';
import 'home.dart';
import 'send.dart';

class PersonScreen extends StatelessWidget {
  final Person person;

  // In the constructor, require a Todo
  PersonScreen({Key key, @required this.person}) : super(key: key);

  final TextStyle textStyle = TextStyle(
      fontSize: 28,
      fontWeight: FontWeight.w400,
      color: Colors.white,
      // shadows: [
      //   BoxShadow(
      //     color: Colors.grey[800],
      //     blurRadius: 3.0, // has the effect of softening the shadow
      //     spreadRadius: 20.0, // has the effect of extending the shadow
      //     offset: Offset(
      //       1.0, // horizontal, move right 10
      //       1.0, // vertical, move down 10
      //     ),
      //   )
      // ]
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        brightness: Brightness.dark,
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Text(person.name),
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
              height: 200,
              child: ChildrenWidget(
                person: person,
              )),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16, top: 20),
            child: SizedBox(
                height: 90,
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xffc6d374),
                      // border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(16.0),
                          topRight: Radius.circular(16))),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      children: <Widget>[
                        Text("Balance:        ", style: textStyle),
                        Text(person.balance, style: textStyle),
                      ],
                    ),
                  ),
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, right: 16),
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  // border: Border.all(color: Colors.grey),
                ),
                height: 240,
                child: TransactionWidget(
                  items: person.transactions,
                )),
          ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () {
       Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SendScreen(
                      person: person,
                    )),
          );

        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), //
    );
  }
}

class ChildrenWidget extends StatelessWidget {
  final Person person;

  // In the constructor, require a Todo
  ChildrenWidget({Key key, @required this.person}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        ChildrenItem(
          pic: person.bigPic,
          width: 350,
        ),
      ],
    );
  }
}

// class Transaction {
//   String title;
//   String amount;

//   Transaction({this.title, this.amount}) : super();
// }

// class TransactionWidget extends StatelessWidget {
//   static List<Transaction> items = [
//     Transaction(title: 'Coop Hauptbahnhof', amount: "15.- CHF"),
//     Transaction(title: 'Hairstylist of the Universe', amount: "116.- CHF"),
//     Transaction(title: 'Hilton Food', amount: "56.- CHF"),
//     Transaction(title: 'Mobility carsharing', amount: "16.- CHF"),
//     Transaction(title: 'Coop Hauptbahnhof', amount: "56.- CHF"),
//     Transaction(title: 'Migros Hauptbahnhof', amount: "56.- CHF"),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return ListView.separated(
//         separatorBuilder: (context, index) => Divider(
//               color: Colors.black87,
//             ),
//         itemCount: items.length,
//         itemBuilder: (BuildContext ctxt, int index) {
//           Transaction transaction = items[index];
//           return ListTile(
//             // leading: Icon(transaction.icon),
//             title: Text(transaction.title),
//             subtitle: Text(transaction.amount),
//           );
//         });
//   }
// }
